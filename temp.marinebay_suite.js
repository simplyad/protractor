describe('test marinebay pages and capabilities', function() {
  
  beforeEach(function () {
    browser.ignoreSynchronization = true;
  });

  afterEach(function () {
    browser.ignoreSynchronization = false;
  });

  var EC = protractor.ExpectedConditions;
  var timeout = 20000;

  it('should show boat ad', function() {

    browser.get('http://marinebay.com');

    browser.wait(EC.elementToBeClickable($('#manufacturer')), timeout);

    element(by.css('#manufacturer')).sendKeys('cobia');
    element(by.css('#boat_search')).click();

    var manufacturer = element(by.css('a.boatManufacturer'));
    expect(manufacturer.getText()).toEqual('Cobia Boats');

    var thumbnail = element(by.css('a.boatState'));
    thumbnail.click();

    var manufacturer = element(by.css('.ad_type_and_location'));
    expect(manufacturer.getText()).toEqual('2013 Cobia Boats 21     Stuart FL, 34994');

    var pin = element(by.css('span.marine_bay_pin_boat'));
    expect(pin.getText()).toContain('Save boat listing');

    browser.executeScript('jQuery("a:contains(Back to previous page)")[0].click()');
    browser.wait(EC.elementToBeClickable($('#manufacturer')), timeout);

  });


  /* Example using POM */
  it('should log me in', function() {

    var LoginPage = require('./pages/login-page.js');
    var  login_page = new LoginPage();

    browser.get('http://marinebay.com/my/signin');

    login_page.enterUsername('simplyad.dev@gmail.com');
  
    login_page.enterPassword('dusterG22$#!');

    browser.wait(EC.elementToBeClickable(login_page.loginButton), timeout);

    login_page.clickLogin();

    browser.wait(EC.textToBePresentInElement($('h1'), 'Member Details'), 5000);

  });

  /* Example using POM */
  it('should see dealer pages', function() {

    var DealerPage = require('./pages/dealer-page.js');
    var  dealer_page = new DealerPage();

    dealer_page.visitDealerPage();
  
    browser.wait(EC.elementToBeClickable(dealer_page.searchButton), timeout);

    dealer_page.enterManufacturer('pathfinder');

    dealer_page.submitForm();

    browser.wait(EC.textToBePresentInElement($('a.boatManufacturer'), 'Pathfinder'), 5000);

  });

});
