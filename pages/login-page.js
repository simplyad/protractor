var login_page = function() {
     
    this.usernameTextBox = element(by.css('#username'));
    this.passwordTextBox = element(by.css('#password'));
    this.loginButton = element(by.css('input[value="Member Login"]'));
     
    this.enterUsername = function(name) {
        this.usernameTextBox.sendKeys(name);
    };
     
    this.enterPassword= function(password) {
        this.usernameTextBox.sendKeys(password);
    };
     
    this.clickLogin = function() {
        this.loginButton.click();
    };
};

var LoginPage = (function () {
    function LoginPage() {
        this.usernameTextBox = element(by.css('#username'));
        this.passwordTextBox = element(by.css('#password'));
        this.loginButton = element(by.css('input[value="Member Login"]'));
    }

    LoginPage.prototype.enterUsername = function(name) {
        this.usernameTextBox.sendKeys(name);
    };
     
    LoginPage.prototype.enterPassword = function(password) {
        this.passwordTextBox.sendKeys(password);
    };
     
    LoginPage.prototype.clickLogin = function() {
        this.loginButton.click();
    };

    LoginPage.prototype.visitPage = function () {
        browser.get("/");
    };

    LoginPage.prototype.fillEmail = function (email) {
        this.emailField.sendKeys(email);
    };

    LoginPage.prototype.fillPassword = function (password) {
        if (password == null) {
            password = "password";
        }
        this.passwordField.sendKeys(password);
    };

    LoginPage.prototype.login = function () {
        this.loginButton.click();
    };

    LoginPage.prototype.getCurrentUser = function () {
        return this.currentUser.getText();
    };

    return LoginPage;

})();

module.exports = LoginPage;
