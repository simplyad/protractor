describe('Virtu test for interview', function() {
  
  beforeEach(function () {
    browser.ignoreSynchronization = true;
  });

  afterEach(function () {
    browser.ignoreSynchronization = false;
  });

  var EC = protractor.ExpectedConditions;
  var timeout = 20000;

  /* Example using POM */
  it('log me into gmail', function() {

    var GmailPage = require('./pages/gmail-page.js');
    var  gmail_page = new GmailPage();

    browser.get('https://google.com/mail');

    browser.wait(EC.visibilityOf($('#identifierId')), timeout);
    
    element(by.css('#identifierId')).click();

    element(by.css('#identifierId')).sendKeys('geigermark3@gmail.com');
    
    browser.wait(EC.elementToBeClickable($('#identifierNext')), timeout);

    element(by.css('#identifierNext > content > span')).click();

    browser.wait(EC.visibilityOf($('input[type=password][name=password]')), timeout);
    
    element(by.css('input[type=password][name=password]')).sendKeys('iwgtRjFM93$!');
    
    browser.wait(EC.elementToBeClickable($('#passwordNext > content > span')), timeout);

    browser.sleep(5000);
    
    element(by.css('#passwordNext > content > span')).click();

    browser.wait(EC.visibilityOf($('#gbqfq')), timeout);
    
    //element(by.css('#gbqfq')).sendKeys('Virtu');

    //element(by.css('#gbqfb > span')).click();

    browser.wait(EC.visibilityOf($$('.UI div table tbody tr').get(0)), timeout);

    //element(by.cssContainingText('#\3a bq', 'Virtu')).click();
    //element(by.css('.Cp div table tbody tr')).click();
    $$(".UI div table tbody tr").get(0).click();

    browser.sleep(5000);

    browser.wait(EC.visibilityOf($('img[src*="blue_lock"]')), timeout);

    $$('img[src*="blue_lock"]').get(0).click();
//    element(by.css('img[src$="blue_lock.png"]')).click();

    browser.sleep(5000);

    browser.getAllWindowHandles().then(function (handles) {
        browser.switchTo().window(handles[1]);
        //browser.wait(EC.visibilityOf($('.iconContainer').get(0)), timeout);
        browser.wait(EC.visibilityOf($('#content')), timeout);
        //$$('.iconContainer span.userEmail').click();
        $$('div[data-email="geigermark3@gmail.com"]').click();
        browser.wait(EC.visibilityOf($('a.sendEmailButton')), timeout);
        $$('a.sendEmailButton').click();
        browser.sleep(5000);
        browser.driver.close();
        //element(by.css('#content > div > div > div > div.login-page > a:nth-child(1) > div > span')).click();
        browser.switchTo().window(handles[0]);
        browser.sleep(5000);
        browser.get('https://google.com/mail');
        //browser.wait(EC.visibilityOf($('#gbqfq')), timeout);
        //browser.wait(EC.visibilityOf($$('.UI div table tbody tr').get(0)), timeout);

		//element(by.cssContainingText('#\3a bq', 'Virtu')).click();
		//element(by.css('.Cp div table tbody tr')).click();
		//$$(".UI div table tbody tr").get(0).click();

        //browser.sleep(5000);

        //browser.wait(EC.visibilityOf($$('a[data-saferedirecturl*="accounts.virtru.com"]').get(0)), timeout);
        //browser.wait(EC.visibilityOf($$("table > tbody > tr > td > a[href*='email-activation']").get(0)), timeout);
		//$$("table tbody tr td a[href*='email-activation']").click();
        //browser.switchTo().window(handles[2]);
        //browser.wait(EC.visibilityOf($('#gbqfq')), timeout);
        browser.sleep(10000);
        //browser.wait(EC.visibilityOf($$('.UI div table tbody tr').get(0)), timeout);
		$$(".UI div table tbody tr").get(0).click();
        browser.sleep(10000);

        $$('a[href*=email-activation]').get(0).click()
        browser.sleep(10000);
    });

  });

});
